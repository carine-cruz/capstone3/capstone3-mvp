import { Fragment, useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Table } from 'react-bootstrap';

import UserContext from '../UserContext';
import ReactPaginate from 'react-paginate';

export default function Sidebar({indicator,cart}){
	
 //indicator values: products, cart, admin

 const [printDetails, setPrintDetails] = useState([]);
 const [total, setTotal] = useState(0);
 let tempTotal = 0;

 const admin = localStorage.getItem('admin');
 const token = localStorage.getItem('token');
 const firstName = localStorage.getItem('firstName');

 const {state} = useContext(UserContext);

 //variables for pagination
 const [currentPage, setCurrentPage] = useState(0);
 const [itemsPerPage, setItemsPerPage] = useState(0);

 const screenSize = window.innerWidth;

 useEffect(()=>{

 	cartSummary();

 	//adjust items depending on screenSize
 	if (screenSize <= 1440){
 		setItemsPerPage(6);
 	} else if (screenSize > 1440 && screenSize < 2560 ){
 		setItemsPerPage(9);
 	} else {
 		setItemsPerPage(10);
 	}

 },[cart])

 const cartSummary = (cart) => {

 	if(localStorage.getItem('orders')){
 		 	let orders = JSON.parse(localStorage.getItem('orders'));
 		// if(cart){
 		// 	let orders = JSON.parse(cart)

 		 	setPrintDetails(
 		 		orders.map(product=>{
 		 			tempTotal += product.subTotal;
 		 			return(
 		 				<tr key={product.productId}>
 		 					<td>{product.productName}</td>
 		 					<td>x</td>
 		 					<td>{product.productQty}</td>
 		 				</tr>
 		 			)
 		 		})
 		 	)
 		 	setTotal(tempTotal)
 	} else {
 		if ( token === null ){
 			let temp = [<tr><td colSpan="3"><center>Please log-in to be able to use the cart.</center></td></tr>]; 
 			setPrintDetails(temp);
 		} else {
 			 let temp = [<tr><td colSpan="3"><center>Empty</center></td></tr>]; 
 			 setPrintDetails(temp);
 		}
 	}
 }

 	const handlePageClick = ({selected: selectedPage}) => {
 		setCurrentPage(selectedPage);
 	}

 	const offset = currentPage * itemsPerPage;

 	//total pages
 	const pageCount = Math.ceil(printDetails.length / itemsPerPage);

 	//slice array to display number of items per page
 	const currentPageData = printDetails.slice(offset, offset + itemsPerPage);

	return (
		<Container fluid className="sideColumn">
			<Row>
				<Col>
					<div style={{height:50}}/>
					<h4>Hello, { firstName !== null ? firstName : `guest`}!</h4>
					<hr className="hrCustomization"/>
				</Col>
			</Row>

			{
				indicator === `products` || indicator === `admin`?

					<Fragment>
					{
						indicator === `admin`?
						<Row>
							<span className="sideLink">Product List</span>
						</Row>
						:<Row></Row>
					}
						<Row>
							{
								admin === `true`
									?<Link to={`../all-products/breads`} className="sideLink ml-3">Bread</Link>
									:<Link to={`../products/breads`} className="sideLink ml-3">Bread</Link>
							}
						</Row>
						<Row>
							{
								admin === `true`
									?<Link to={`../all-products/cakes`} className="sideLink ml-3">Cakes</Link>
									:<Link to={`../products/cakes`} className="sideLink ml-3">Cakes</Link>
							}
						</Row>
						<Row>
							{
								admin === `true`
									?<Link to={`../all-products/pies`} className="sideLink ml-3">Pies</Link>
									:<Link to={`../products/pies`} className="sideLink ml-3">Pies</Link>
							}
						</Row>
						<Row>
							{
								admin === `true`
									?<Link to={`../all-products/cheesecakes`} className="sideLink ml-3">Cheesecakes</Link>
									:<Link to={`../products/cheesecakes`} className="sideLink ml-3">Cheesecakes</Link>
							}
						</Row>
						<Row>
							{
								admin === `true`
									?<Link to={`../all-products/bites`} className="sideLink ml-3">Quick Bites</Link>
									:<Link to={`../products/bites`} className="sideLink ml-3">Quick Bites</Link>
							}
						</Row>
					</Fragment>
				:
					<Fragment>
						<h4>Summary</h4>
						<Table className="cartFont">
							<tbody>
								{
									printDetails
									/*currentPageData*/
								}
								{
									token === null 
									? <Fragment></Fragment> 
									:<tr>
										<td><strong>Total</strong></td>
										<td></td>
										<td>&#8369;{total.toFixed(2)}</td>
									</tr>
								}
							</tbody>
						</Table>
						{
		/*					printDetails.length < 6 ? <></>
							: <ReactPaginate
								previousLabel={"<"}
								nextLabel={">"}
								pageCount={pageCount}
								containerClassName="pagination"
								onPageChange={()=>handlePageClick}
								pageClassName={"ml-1 px-2 blackOpaque"}
								nextClassName={"ml-1 px-2 whiteOpaque"}
								previousClassName={"ml-1 px-2 whiteOpaque"}
								previousLinkClassName={"sideLink"}
								nextLinkClassName={"sideLink"}
								disabledLinkClassName={"ml-1 disabledPageLink"}
								activeClassName={"tableHeader"}
							/> */
						}
						{
							token === null ? <Fragment></Fragment> : <Link to={'../cart'} className="btn tableBtn">Update cart</Link>
						}
						
					</Fragment>

			}

			{
				indicator === `admin` ?

				<Fragment>
					<hr className="hrCustomization"/>

					<Row className="mt-4">
						<Link to={`../order-history`} className="sideLink">Orders</Link>
					</Row>

					<Row>
						<Link to={`../all-users`} className="sideLink">Users</Link>
					</Row>
				</Fragment>

				:
				<Fragment></Fragment>
			}
		</Container>
	)
}