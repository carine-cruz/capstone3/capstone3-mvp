import {Container, Row, Col, Button, Table} from 'react-bootstrap';
import {useState, useEffect, useContext } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import ReactPaginate from 'react-paginate';

import ProductCard from '../components/ProductCard';
import Sidebar from '../components/Sidebar';
import UserContext from '../UserContext';
import toastr from 'toastr';


const admin = localStorage.getItem('admin');
const firstName = localStorage.getItem('firstName');

export default function ProductList(){

	const [products, setProducts] = useState([]);
	const [breads, setBreads] = useState([]);
	const [cakes, setCakes] = useState([]);
	const [bites, setBites] = useState([]);
	const [cheesecakes, setCheesecakes] = useState([]);
	const [pies, setPies] = useState([]);

	//variables for pagination
	const [currentPage, setCurrentPage] = useState(0);
	const [itemsPerPage, setItemsPerPage] = useState(0);

	const navigate = useNavigate();
	const {category} = useParams();
	const { state, dispatch } = useContext(UserContext);

	const screenSize = window.innerWidth;

	//retrieve products upon page load
	useEffect(()=>{

		if (admin === "true"){
			dispatch({type:"ADMIN", name: firstName, admin: true});
		} else if (admin === "false") {
			dispatch({type:"USER", name: firstName, admin: false});
		}

		if (state.admin === true){
			fetchData();
		} else {
			navigate(`../products`);
		}

		if (screenSize <= 1440){
			setItemsPerPage(5);
		} else if (screenSize > 1440 && screenSize < 2560 ){
			setItemsPerPage(8);
		} else {
			setItemsPerPage(16);
		}

	},[])

	const fetchData = () => {
		
		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/products/all-products`, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(result => {

			let temp = [];

			result.forEach(product => {
				if(product.category.toLowerCase() === `bread`){
					temp.push(
						<tr key={product._id}>
							<td style={{display:'none'}}>{product._id}</td>
							<td>{product.name}</td>
							<td>&#8369; {product.price.toFixed(2)}</td>
							<td>{product.stockCount}</td>
							<td>{product.quantitySold}</td>
							<td>
							{
								product.isActive
								?<Button 
									variant="danger"
									onClick={() => updateStatus(product._id)}
								>Deactivate</Button>
								:<Button 
									variant="success"
									onClick={() => updateStatus(product._id)}
								>Activate</Button>
							}
							</td>
							<td>
								<Button 
									variant="light" 
									onClick={()=>{
										console.log(`why not navigate?`)
										navigate(`../update-product/${product._id}`)}}
								>Update</Button>
							</td>
						</tr>
					)
				}
			});

			setBreads(temp);
			temp = [];

			result.forEach(product=>{
				if(product.category.toLowerCase() === `pies`){
					temp.push(
						<tr key={product._id}>
							<td style={{display:'none'}}>{product._id}</td>
							<td>{product.name}</td>
							<td>&#8369; {product.price.toFixed(2)}</td>
							<td>{product.stockCount}</td>
							<td>{product.quantitySold}</td>
							<td>
							{
								product.isActive
								?<Button 
									variant="danger"
									onClick={() => updateStatus(product._id)}
								>Deactivate</Button>
								:<Button 
									variant="success"
									onClick={() => updateStatus(product._id)}
								>Activate</Button>
							}
							</td>
							<td>
								<Button variant="light"
										onClick={()=>navigate(`../update-product/${product._id}`)}
								>Update</Button>
							</td>
						</tr>
					)
				}
			});
			setPies(temp);
			temp = [];

			result.forEach(product=>{
				if(product.category.toLowerCase() === `cheesecakes`){
					temp.push(
						<tr key={product._id}>
							<td style={{display:'none'}}>{product._id}</td>
							<td>{product.name}</td>
							<td>&#8369; {product.price.toFixed(2)}</td>
							<td>{product.stockCount}</td>
							<td>{product.quantitySold}</td>
							<td>
							{
								product.isActive
								?<Button 
									variant="danger"
									onClick={() => updateStatus(product._id)}
								>Deactivate</Button>
								:<Button variant="success">Activate</Button>
							}
							</td>
							<td>
								<Button 
									variant="light"
									onClick={()=>navigate(`../update-product/${product._id}`)}
								>Update</Button>
							</td>
						</tr>
					)
				}
			});

			setCheesecakes(temp);
			temp = [];

			result.forEach(product=>{
				if(product.category.toLowerCase() === `cakes`){
					temp.push(
						<tr key={product._id}>
							<td style={{display:'none'}}>{product._id}</td>
							<td>{product.name}</td>
							<td>&#8369; {product.price.toFixed(2)}</td>
							<td>{product.stockCount}</td>
							<td>{product.quantitySold}</td>
							<td>
								{
									product.isActive
									?<Button 
										variant="danger"
										onClick={() => updateStatus(product._id)}
									>Deactivate</Button>
									:<Button 
										variant="success"
										onClick={() => updateStatus(product._id)}
									>Activate</Button>
								}
							</td>
							<td>
								<Button 
									variant="light"
									onClick={()=>navigate(`../update-product/${product._id}`)}
								>Update</Button>
							</td>
						</tr>
					)
				}
			})
			setCakes(temp);
			temp = [];

			result.forEach(product=>{
				if(product.category.toLowerCase() === `quick bites`){
					temp.push(
						<tr key={product._id}>
							<td style={{display:'none'}}>{product._id}</td>
							<td>{product.name}</td>
							<td>&#8369; {product.price.toFixed(2)}</td>
							<td>{product.stockCount}</td>
							<td>{product.quantitySold}</td>
							<td>
							{
								product.isActive
								?<Button 
									variant="danger"
									onClick={() => updateStatus(product._id)}
								>Deactivate</Button>
								:<Button 
									variant="success"
									onClick={() => updateStatus(product._id)}
								>Activate</Button>
							}
							</td>
							<td>
								<Button 
									className="tableBtn"
									onClick={()=>navigate(`../update-product/${product._id}`)}
								>Update</Button>
							</td>
						</tr>
					)
				}
			})
			setBites(temp);
			temp=[];

		}) // END THEN

	}

	//function to update product status
	const updateStatus = (productId) => {
	
		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/products/active-status/${productId}`, {
			method: "PATCH",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(result => {
			if(result){
				fetchData();
			} else {
				toastr.error(result.message);
				// alert(result.message);
			}
		})
	}

	const getTitle = () => {
		switch(category){
			case `breads`: return `BREAD INVENTORY`;
							break;
			case `pies`: return `PIES INVENTORY`;
							break;
			case `bites`: return `QUICK BITES INVENTORY`;
							break;
			case `cakes`: return `CAKES INVENTORY`;
							break;
			case `cheesecakes`: return `CHEESECAKES INVENTORY`;
							break;
			default: return `CAKES INVENTORY`;
		}
	}

	const getDisplay = () => {
		switch(category){
			case `breads`: return breads;
							break;
			case `pies`: return pies;
							break;
			case `bites`: return bites;
							break;
			case `cakes`: return cakes;
							break;
			case `cheesecakes`: return cheesecakes;
									break;
			default: return cakes;
		}
	}

	const handlePageClick = ({selected: selectedPage}) => {
		setCurrentPage(selectedPage);
	}

	const offset = currentPage * itemsPerPage;

	//total pages
	const pageCount = Math.ceil(getDisplay().length / itemsPerPage);

	//slice array to display number of items per page
	const currentPageData = getDisplay().slice(offset, offset + itemsPerPage)

	//display page --------------------------------------------
	return(
		<Container fluid className="adminPage">
			<Row>
				<Col md={2} className="sideColumn ">
					<Sidebar indicator={`admin`}/>
				</Col>

				<Col md={10}>
					<div style={{height:60}}/>
					<h3><span className="loginLabel blackOpaque">{getTitle()}</span></h3>
					<Link to={`../new-product`} className="btn my-3 tableBtn">Add new product</Link>
					<Table className="tableBackground" responsive >
						<thead>
							<tr className="tableHeader">
								<th style={{display:'none'}}>ID</th>
								<th>Name</th>
								<th>Price</th>
								<th>Stocks</th>
								<th># Sold</th>
								<th>Availability</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							{/*getDisplay()*/
							currentPageData
							}
						</tbody>
					</Table>

					<center><ReactPaginate
						previousLabel={"<"}
						nextLabel={">"}
						pageCount={pageCount}
						containerClassName="pagination"
						onPageChange={()=>handlePageClick}
						pageClassName={"ml-1 px-2 blackOpaque"}
						nextClassName={"ml-1 px-2 whiteOpaque"}
						previousClassName={"ml-1 px-2 whiteOpaque"}
						previousLinkClassName={"sideLink"}
						nextLinkClassName={"sideLink"}
						disabledLinkClassName={"ml-1 disabledPageLink"}
						activeClassName={"tableHeader"}
					/></center>

				</Col>
			</Row>	
		
		</Container>
	)

}