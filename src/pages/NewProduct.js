import {useState, useEffect} from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Container, Row, Form, Col, Button, DropdownButton, Dropdown, InputGroup} from 'react-bootstrap';
import ImageCarousel from '../components/ImageCarousel';

import toastr from 'toastr';

export default function NewProduct(){
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [category, setCategory] = useState(`Select category`);
	const [stocks, setStocks] = useState(0);
	// const [isActive, setIsActive] = useState("");

	const {productId} = useParams();

	const navigate = useNavigate();

	if (localStorage.getItem('admin') !== "true"){
		navigate(`../products`);
	}


	const addProduct = (e) =>{
		e.preventDefault();

		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/products/new-product`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				category: category,
				stockCount: stocks 
			})
		})
		.then(result => result.json())
		.then(result => {

			if (result){
				toastr.success(`New product created.`);
				// alert(`New product created.`);
				navigate(`../all-products/cakes`);
			} else {
				toastr.error(result.message);
				// alert(result.message)
			}
		})
	}

	const handleDDown = (e) => {
		setCategory(e);
	}

	return (
		<Container fluid className="adminPage min-vh-100 mx-auto">
			
			<Row className="fluid mx-auto my-auto">
				<Col md={5} className="my-auto ml-auto">
					<ImageCarousel/>
					<center><Button className="mainButton my-2 tableBtn" variant="">Upload Picture</Button></center>
				</Col>
				<Col className="my-auto tableBackground p-3 mr-auto" md={3}>
					<h3 className="loginLabel2">NEW PRODUCT</h3>
					<Form onSubmit={(e) => addProduct(e)}>
					  <Form.Group className="mb-3">
					  	<Form.Control 
					  		type="text" 
					  		className="login2" 
					  		value={name}
					  		placeholder="Product name" 
					  		onChange={(e)=> setName(e.target.value)}
					  		required
					  	/>
					  </Form.Group>

					  <Form.Group className="mb-3">
					  <InputGroup>
					  	<InputGroup.Text className="igText">&#8369;</InputGroup.Text>
					    <Form.Control 
					    	type="number" 
					    	className="login2" 
					    	value={price}
					    	placeholder="Price"
					    	onChange={(e)=> setPrice(e.target.value)}
					    	required
					    />
					    </InputGroup>
					    </Form.Group>
					  

					  <Form.Group className="mb-3">
					    <Form.Control 
					    	as="textarea" 
					    	className="login2" 
					    	value={description}
					    	placeholder="Description"
					    	onChange={(e)=> setDescription(e.target.value)}
					    	row="3"
					    	required
					    />
					   
					  </Form.Group>

					  <Form.Group className="mb-3">
							<DropdownButton 
								id="prodDropdown"
								title={category} 
								className="btn-block mainButton2"
								onSelect={handleDDown} >
							    <Dropdown.Item eventKey="Bread" className="dropdownBg">Bread</Dropdown.Item>
							    <Dropdown.Item eventKey="Pies" className="dropdownBg">Pies</Dropdown.Item>
							    <Dropdown.Item eventKey="Cakes" className="dropdownBg">Cakes</Dropdown.Item>
							    <Dropdown.Item eventKey="Cheesecakes" className="dropdownBg">Cheesecakes</Dropdown.Item>
							    <Dropdown.Item eventKey="Quick Bites" className="dropdownBg">Quick Bites</Dropdown.Item>
							</DropdownButton>
					  </Form.Group>

					  <Form.Group className="mb-3">
					  	<InputGroup>
					  		<InputGroup.Text className="igText">Stocks</InputGroup.Text>
					    <Form.Control 
					    	type="number" 
					    	className="login2" 
					    	value={stocks}
					    	placeholder="Stocks"
					    	onChange={(e)=> setStocks(e.target.value)}
					    	required
					    />
					    </InputGroup>
					  </Form.Group>

					  <center>
					  	<Button variant="outline-light" type="submit" className="mainButton2">
					    Submit
					  	</Button>
					  	<Button 
					  		variant="outline-light" 
					  		type="submit" 
					  		className="mainButton2 m-2" 
					  		onClick={()=>navigate(`../all-products/cakes`)}>
					  	  Cancel
					  	</Button>
					  </center>
					</Form>
				</Col>
			</Row>
		</Container>
	)
}