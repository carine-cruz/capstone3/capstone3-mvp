import { useParams, Link } from 'react-router-dom';
import { useEffect, useState, Fragment } from 'react';
import { Container, Row, Col, Table, Button, Form, InputGroup } from 'react-bootstrap';

import toastr from 'toastr';

export default function Profile(){

	const {userId} = useParams();

	const admin = localStorage.getItem('admin');
	const token = localStorage.getItem('token');

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [isAdmin, setIsAdmin] = useState(false);
	const [mobileNo, setMobileNo] = useState("");
	const [isActive, setIsActive] = useState(true);
	const [myId, setMyId] = useState("")

	//from Registration, validation of email
	const [isValid, setIsValid] = useState(true);
	const [isLoading, setIsLoading] = useState("noLoad");

	useEffect(()=>{
		fetchData();
	},[])

	const fetchData = () => {

		let fetchAPI = "";

		if (admin === "true"){
			fetchAPI = `https://tranquil-refuge-66470.herokuapp.com/api/users/profile/${userId}`
		} else {
			fetchAPI = `https://tranquil-refuge-66470.herokuapp.com/api/users/my-profile`
		}

		fetch(fetchAPI, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			if (result.message !== null){
				setFirstName(result.firstName);
				setLastName(result.lastName);
				setEmail(result.email);
				setIsAdmin(result.isAdmin);
				setMobileNo(result.mobileNo.toString());
				setIsActive(result.isActive);
				setMyId(result._id);
			} else {
				toastr.error(result.message);
				// alert(result.message);
			}
		})
	}

	const getBackground = () => {
		if (admin === "true"){
			return `adminPage my-auto`;
		} else {
			return `welcome my-auto`;
		}
	}

	const getMargin = () => {
		if (admin === "true"){
			return `my-auto mx-auto`;
		} else {
			return `my-auto ml-5`;
		}
	}

	const getColumn = () => {
		if (admin === "true"){
			return 6;
		} else {
			return 4;
		}
	}

	const updateAdminStatus = (userId) => {
		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/users/change-user-status/${userId}`, {
			method: "PATCH",
			headers: { "Authorization": `Bearer ${token}` }
		})
		.then(result => result.json())
		.then(result => {
			if (result.message !== null){
				toastr.success(`Administrator status updated successfully.`)
				// alert(`Administrator status updated successfully.`);
				fetchData();
			} else {
				toastr.error(result.message)
				// alert(result.message);
			}
		})
	}

	const updateActiveStatus = (userId) => {
		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/users/user-access/${userId}`, {
			method: "PATCH",
			headers: { "Authorization": `Bearer ${token}` }
		})
		.then(result => result.json())
		.then(result => {
			if (result.message !== null){
				toastr.success(`Active status updated successfully.`)
				// alert(`Active status updated successfully.`);
				fetchData();
			} else {
				toastr.error(result.message)
				// alert(result.message);
			}
		})
	}

	const validateEmailCSS = () => {
		if(isValid){
			return "login";
		} else {
			return "login loginFailed";
		}
	}

	const validateEmail = (emailAddress) => {
		
		setIsLoading("load");
		
		fetch('https://tranquil-refuge-66470.herokuapp.com/api/users/email-exists', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(response => {
			if (response){
				setIsValid(false);
				toastr.warning('Email already registered.');
				// alert('Email already registered.');
			} else {
				setIsValid(true);
			}
			setIsLoading("noLoad");
		})
	}

	const displayLoading = () => {
		
		if (isLoading === "load"){
			return "spinner-border spinner-border-sm mx-1";
		} else if (isLoading === "noLoad"){
			return null;
		}
	}

	const updateProfile = (e) => {
		e.preventDefault();

		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/users/update/${myId}`, {
			method: "PUT",
			headers: {
				"Authorization": `Bearer ${token}`,
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo
			})
		})
		.then(result => result.json())
		.then(result => {
			if(result.message !== null){
				toastr.success(`User details successfully updated.`);
				// alert(`User details successfully updated.`);
			} else {
				toastr.warning(result.message);
				// alert(result.message);
			}
		})

	}

	const getDisplay = () => {
		if (admin === "true"){
			return(
			<Fragment>
				<Table className="tableBackground mt-4">
					<thead className="tableHeader">
					     <tr>
							<th></th>
							<th></th>
					    </tr>
					</thead>
					<tbody>
						<tr>
							<td><strong>First Name:</strong></td>
							<td>{firstName}</td>
						</tr>
						<tr>
							<td><strong>Last Name:</strong></td>
							<td>{lastName}</td>
						</tr>
						<tr>
							<td><strong>Email Address:</strong></td>
							<td>{email}</td>
						</tr>
						<tr>
							<td><strong>Mobile Number:</strong></td>
							<td>{mobileNo}</td>
						</tr>
						<tr>
							<td><strong>Administrator:</strong></td>
							<td>{
								isAdmin ? 
								<Button 
									variant="danger"
									onClick={() => updateAdminStatus(userId)}
								>Demote</Button>
								:<Button 
									variant="success"
									onClick={() => updateAdminStatus(userId)}
								>Promote</Button>
							}</td>
						</tr>
						<tr>
							<td><strong>Active:</strong></td>
							<td>{
								isActive?
								<Button 
									variant="danger"
									onClick={() => updateActiveStatus(userId)}
								>Deactivate</Button>
								:<Button 
									variant="success"
									onClick={() => updateActiveStatus(userId)}
								>Activate</Button>
							}</td>
						</tr>
						<tr className="tableHeader">
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</Table>
				<Link to={`../all-users`} className="btn tableBtn mt-3">Back to list</Link>
			</Fragment>
			)
		} else {
			return(
			<Fragment>
				<Form onSubmit={(e) => updateProfile(e)} className="mt-3">

					<Form.Group className="mb-3">
							<Form.Control 
								type="text" 
								className="login" 
								value={firstName}
								onChange={(e)=> setFirstName(e.target.value)}
							/>
					</Form.Group>

					<Form.Group className="mb-3">
							<Form.Control 
								type="text" 
								className="login" 
								value={lastName}
								onChange={(e)=> setLastName(e.target.value)}
							/>
					</Form.Group>

					<Form.Group className="mb-3">
						<Form.Control 
							type="email" 
							className={validateEmailCSS()}
							value={email}
							onChange={(e)=> setEmail(e.target.value)}
							onBlur={(e)=>validateEmail({email})}
						/>
						<span className={displayLoading()}></span>
					</Form.Group>

					<Form.Group className="mb-3">
						<Form.Control 
							type="text" 
							className="login"
							value={mobileNo}
							onChange={(e)=> setMobileNo(e.target.value)}
						/>
					</Form.Group>

				  	<center><Button variant="outline-light" type="submit" className="mainButton">
				    Update
				  	</Button></center>
				</Form>
				<center><Button variant="outline-light" className="mainButton mt-3">Reset Password</Button></center>
			</Fragment>
			)
		}
	}

	return(
		<Container fluid className={getBackground()}>
			<div style={{height:80}}/>
			<Row className="my-auto">
				<Col className={getMargin()} md={getColumn()}>
					<center><h4><span className="loginLabel tableBackground my-2">USER ID: {myId}</span></h4></center>
					{getDisplay()}
				</Col>
			</Row>
		</Container>
	)
}