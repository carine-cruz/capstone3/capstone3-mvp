import { useEffect, useState, useContext, Fragment } from 'react';
import { Container, Row, Col, Table, Button } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';

import Sidebar from '../components/Sidebar';
import UserContext from '../UserContext';
import ReactPaginate from 'react-paginate';
import toastr from 'toastr';

export default function UserList(){

	const [users, setUsers] = useState("");

	const navigate = useNavigate();
	const { state } = useContext(UserContext);

	//retrieve local storage variables
	const token = localStorage.getItem('token');
	const admin = localStorage.getItem('admin');

	//variables for pagination
	const [currentPage, setCurrentPage] = useState(0);
	const [itemsPerPage, setItemsPerPage] = useState(0);

	const screenSize = window.innerWidth;

	useEffect(()=>{

		if ( admin !== "true" ){
			navigate(`/`);
		}

		fetchData();

		if (screenSize <= 1440){
			setItemsPerPage(7);
		} else if (screenSize > 1440 && screenSize < 2560 ){
			setItemsPerPage(10);
		} else {
			setItemsPerPage(16);
		}

	},[])

	const fetchData = () => {

		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/users/all-users`, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
				
			if (result) {
				setUsers(result.map(user => {
					return(formatData(user))
				}));
			} else {
				setUsers([<tr><td ColSpan="8">User list empty</td></tr>]);
			}

		})
	}

	const updateAdminStatus = (userId) => {
		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/users/change-user-status/${userId}`, {
			method: "PATCH",
			headers: { "Authorization": `Bearer ${token}` }
		})
		.then(result => result.json())
		.then(result => {
			if (result.message !== null){
				toastr.success(`Administrator status updated successfully.`);
				// alert(`Administrator status updated successfully.`);
				fetchData();
			} else {
				toastr.error(result.message);
				// alert(result.message);
			}
		})
	}

	const updateActiveStatus = (userId) => {
		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/users/user-access/${userId}`, {
			method: "PATCH",
			headers: { "Authorization": `Bearer ${token}` }
		})
		.then(result => result.json())
		.then(result => {
			if (result.message !== null){
				toastr.success(`Active status updated successfully.`);
				// alert(`Active status updated successfully.`);
				fetchData();
			} else {
				toastr.error(result.message);
				// alert(result.message);
			}
		})
	}

	const formatData = (user) => {
		return(
			<tr key={user._id} className="p-1">
				<td className="p-2">{user.firstName}</td>
				<td className="p-2">{user.lastName}</td>
				<td className="p-2">{user.email}</td>
				<td className="p-2">{user.mobileNo}</td>
				<td className="p-2">{
					user.isAdmin ? 
					<Button 
						variant="danger"
						onClick={() => updateAdminStatus(user._id)}
					>Demote</Button>
					:<Button 
						variant="success"
						onClick={() => updateAdminStatus(user._id)}
					>Promote</Button>
				}</td>
				<td className="p-2">{
					user.isActive?
					<Button 
						variant="danger"
						onClick={() => updateActiveStatus(user._id)}
					>Deactivate</Button>
					:<Button 
						variant="success"
						onClick={() => updateActiveStatus(user._id)}
					>Activate</Button>
				}</td>
				<td className="p-2">
					<Button className="tableBtn"
						onClick={()=>navigate(`../profile/${user._id}`)}
					>View</Button>
				</td>
			</tr>
		)
	}

	const handlePageClick = ({selected: selectedPage}) => {
		setCurrentPage(selectedPage);
	}

	const offset = currentPage * itemsPerPage;

	//total pages
	const pageCount = Math.ceil(users.length / itemsPerPage);

	//slice array to display number of items per page
	const currentPageData = users.slice(offset, offset + itemsPerPage)

	return(
		<Container fluid className="adminPage" style={{overflowY: 'auto'}}>
			<Row>
				{
					admin === "true" ?
					<Col md={2} className="sideColumn ">
						<Sidebar indicator={`admin`}/>
					</Col>
					: <Fragment></Fragment>
				}
				<Col md={10} className="mx-auto">
					<div style={{height:60}}/>
					<h3><center><span className="loginLabel blackOpaque">USERS LIST</span></center></h3>
					<Table variant="striped" className="tableBackground mt-4">
						<thead>
							<tr className="p-2 tableHeader">
								<th className="p-2">First Name</th>
								<th className="p-2">Last Name</th>
								<th className="p-2">Email Address</th>
								<th className="p-2">Mobile Number</th>
								<th className="p-2">Administrator?</th>
								<th className="p-2">Active?</th>
								<th className="p-2">Action</th>
							</tr>
						</thead>
						<tbody>
							{
								users
								/*currentPageData*/
							}
						</tbody>
					</Table>
{/*					<center><ReactPaginate
						previousLabel={"<"}
						nextLabel={">"}
						pageCount={pageCount}
						containerClassName="pagination"
						onPageChange={()=>handlePageClick}
						pageClassName={"ml-1 px-2 blackOpaque"}
						nextClassName={"ml-1 px-2 whiteOpaque"}
						previousClassName={"ml-1 px-2 whiteOpaque"}
						previousLinkClassName={"sideLink"}
						nextLinkClassName={"sideLink"}
						disabledLinkClassName={"ml-1 disabledPageLink"}
						activeClassName={"tableHeader"}
					/></center>*/}
				</Col>
			</Row>
		</Container>
	)

}