import { Navigate } from 'react-router-dom';
import { useEffect, useContext } from 'react';
import toastr from 'toastr';

// import UserContext from '../UserContext';

export default function Logout(){

	// const {user} = useContext(UserContext);

	useEffect(()=>{
		// console.log(user);
		localStorage.clear();
		toastr.success(`Log-out successful.`);
		// alert(`Log-out successful`);
	},[])

	return <Navigate to={`/`}/>

}