import { useParams, useNavigate, Link } from 'react-router-dom';
import { Container, Row, Col, Button, Table } from 'react-bootstrap';
import { useEffect, useState, Fragment } from 'react';

import moment from 'moment';
import toastr from 'toastr';

const admin = localStorage.getItem('admin')

export default function Order(){

	const {orderId} = useParams();

	const [orderedBy, setOrderedBy] = useState("");
	const [totalAmount, setTotalAmount] = useState(0);
	const [orderItems, setOrderItems] = useState([]);
	const [orderStatus, setOrderStatus] = useState("");
	const [paymentStatus, setPaymentStatus] = useState("");
	const [orderFulfilled, setOrderFulfilled] = useState("");
	const [orderedById, setOrderedById] = useState("");
	const [orderPlaced, setOrderPlaced] = useState("");
	const [paymentDate, setPaymentDate] = useState("");

	const navigate = useNavigate();

	useEffect(() => {


		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/orders/order/${orderId}`, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(result => {
			if(result){
				setOrderedBy(result.orderedBy);
				setOrderedById(result.OrderedById);
				setTotalAmount(result.totalAmount);
				setOrderItems(result.orderItems);
				setOrderStatus(result.orderStatus);
				setPaymentStatus(result.paymentStatus);
				setOrderFulfilled(result.orderFulfilled);
				setOrderPlaced(result.purchasedOn);
				setPaymentDate(result.paymentDate);
			} else {
				toastr.error(result.message);
				// alert(result.message);
				navigate('./error');
			}
		})

	},[])

	const getOrderDetails = () => {
		let formattedArr = orderItems.map(product=> {
			return (
			<tr key={product.productId}>
				<td>{product.productName}</td>
				<td>&#8369;{product.productPrice.toFixed(2)}</td>
				<td>{product.productQty}</td>
				<td>&#8369;{product.subTotal.toFixed(2)}</td>
			</tr>	

			)
		})

		return formattedArr
	}

	const orderBackground = () =>{
		if (admin === "true"){
			return "adminPage"
		} else {
			return "welcome"
		}
	}

	return(
		<Container fluid className={orderBackground()} style={{overflowY:'scroll'}}>
			<div style={{height:60}}/>
			<Row>
				<Col className="my-auto mb-4 mx-auto" md={6}>
					<h4><center><span className="loginLabel">ORDER: {orderId}</span></center></h4>
					<Table className="mt-3 tableBackground">
						<tbody>
							<tr className="whiteOpaque">
								<td><strong>Ordered By:</strong></td>
								<td><Link to={`../profile/${orderedById}`}>{orderedBy}</Link></td>
								<td><strong>Order Placed:</strong></td>
								<td>{moment(orderPlaced).format('MM-DD-YYYY HH:mm')}</td>
							</tr>
							<tr className="whiteOpaque">
								<td><strong>Order Status:</strong></td>
								<td>{orderStatus}</td>
								<td><strong>Payment Date:</strong></td>
								<td>
									{
										paymentDate === undefined ?
										`------`
										: moment(paymentDate).format('MM-DD-YYYY HH:mm')
									}
								</td>
							</tr>
							<tr className="whiteOpaque">
								<td><strong>Payment Status:</strong></td>
								<td>{paymentStatus}</td>
								<td><strong>Order Completed Date:</strong></td>
								<td>
									{
										orderFulfilled === undefined ?
										`------`
										: moment(orderFulfilled).format('MM-DD-YYYY HH:mm')
									}
								</td>
							</tr>
							<tr>
								<td colSpan="4" className="tableHeader">
									<center><h5>Items:</h5></center>
								</td>
							</tr>
							<tr>
								<td>Product</td>
								<td>Price</td>
								<td>Quantity</td>
								<td>Subtotal</td>
							</tr>
							
							{getOrderDetails()}

							<tr>
								<td colSpan="4"><center><h5></h5></center></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><strong>Total Amount</strong></td>
								<td>&#8369;{totalAmount.toFixed(2)}</td>
							</tr>

						</tbody>
					</Table>
				</Col>
			</Row>
			<Row>
				<Col>
					{	admin === "true"
						?<Button class="btn tableBtn" onClick={()=>navigate('/order-history')}>Back to list</Button>
						:<Button class="btn tableBtn" onClick={()=>navigate('../products/cakes')}>Back to products</Button>
					}
				</Col>
			</Row>
		</Container>
	)
}