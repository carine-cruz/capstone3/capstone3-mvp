

export default function About(){
	return(<p>The Bakery is a premium bakeshop founded on 2015 in Manila, Philippines that offers high quality handmade cakes, breads and pastries.

For 7 years, The Bakery has been a favorite snack an dessert to homes especially during celebrations. It has also been popular to both foreign and local regulars.</p>)
}
